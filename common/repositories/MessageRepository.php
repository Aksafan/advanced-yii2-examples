<?php

namespace common\repositories;

use common\models\Messages;

class MessageRepository
{
    /**
     * Finds the Messages model based on its primary key value.
     *
     * @param integer $id
     *
     * @return null|Messages the loaded model
     */
    public function findMessage(int $id): ?Messages
    {
        return Messages::findOne($id);
    }

    public function findBannedMessages()
    {
        return Messages::findAll(['id' => [1,2,3,4,5,6,7,8,9,10]]);
    }

    /**
     * @param Messages $message
     *
     * @return bool
     */
    public function save(Messages $message): bool
    {
        if (! $message->save()) {
            \Yii::error('Error has been occurred while saving Message model. Errors = ' . json_encode($message->getErrors()) . '. Attributes = ' . json_encode($message->getAttributes()), __METHOD__);

            return false;
        }

        return true;
    }
}