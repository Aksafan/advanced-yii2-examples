<?php

namespace common\repositories;

use common\models\Chats;
use common\models\Item;
use common\models\Messages;
use common\models\User;

class UserRepository
{
    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return User
     */
    public function findByUsername($username)
    {
        return User::findOne(['username' => $username, 'status' => User::STATUS_ACTIVE]);
    }
}