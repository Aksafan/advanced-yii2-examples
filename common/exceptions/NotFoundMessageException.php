<?php

declare(strict_types=1);

namespace common\exceptions;

use DomainException;

class NotFoundMessageException extends DomainException
{
    protected $message = 'The requested message does not exist.';
}