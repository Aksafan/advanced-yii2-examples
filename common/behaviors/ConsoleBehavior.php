<?php

namespace common\behaviors;

use common\models\User;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;

class ConsoleBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'log'
        ];
    }

    public function log(Event $event)
    {
        /** @var User $user */
        $user = $event->sender;
        $user->id;
        echo ' wefwe';
    }
}