<?php

namespace common\behaviors;

use common\models\User;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;

class UserBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            User::EVENT_BEFORE_CONSOLE_COMMAND_EXECUTE => 'log',
        ];
    }

    public function log(Event $event)
    {
        /** @var User $user */
        $user = $event->sender;
        $user->id;
        echo ' wefwe';
    }
}