<?php

namespace common\components;

use common\models\User;
use http\Exception\InvalidArgumentException;
use Yii;
use yii\base\Component;
use yii\db\Transaction;

class TransactionComponent extends Component
{
    public $url;

    public function init()
    {
        if (! $this->url) {
            throw new InvalidArgumentException();
        }
    }

    public function testTransaction()
    {
        $transaction = User::getDb()->beginTransaction();
        if (doSomeLogic()) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
            Yii::error('Errors has been occured during transaction process', 'transaction-error');
        }
    }

    public function testTransactionWithException()
    {
        $transaction = User::getDb()->beginTransaction();
        try {
            doSomeLogic();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::error('Errors has been occured during transaction process', 'transaction-error');
            throw $e;
        }
    }
}