<?php

namespace common\components;

use common\models\LoginForm;
use common\repositories\UserRepository;
use Yii;

class UserService
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @param LoginForm $model
     *
     * @return bool whether the user is logged in successfully
     */
    public function login(LoginForm $model)
    {
        $userByUsername = $this->userRepository->findByUsername($model->username);
        if (! $userByUsername) {
            return false;
        }

        return Yii::$app->user->login($userByUsername, $model->rememberMe ? 3600 * 24 * 30 : 0);
    }
}