<?php

namespace common\components;

use common\exceptions\NotFoundMessageException;
use common\models\forms\CreateMessageForm;
use common\models\Messages;
use common\models\ServiceStatusId;
use common\repositories\MessageRepository;

class MessageService
{
    /** @var MessageRepository $messageRepository */
    private $messageRepository;

    /**
     * MessageService constructor.
     *
     * @param MessageRepository $messageRepository
     */
    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param CreateMessageForm $createMessageForm
     *
     * @return bool
     */
    public function createMessage(CreateMessageForm $createMessageForm): bool
    {
        $message = new Messages();
        $message->load($createMessageForm->getAttributes(), '');
        $message->id = Uuid::uuid4()->toString();
        $message->status_id = ServiceStatusId::STATUS_BEFORE_ACTIVE;
        if ($this->messageRepository->save($message)) {
            return true;
        }
        $createMessageForm->addErrors($message->getErrors());
        \Yii::error('', __METHOD__);

        return false;
    }

    /**
     * Finds the Messages model based on its primary key value.
     *
     * @param integer $id
     *
     * @return Messages the loaded model
     *
     * @throws NotFoundMessageException if the model cannot be found
     */
    public function getMessageById(int $id): Messages
    {
        if ($model = $this->messageRepository->findMessage($id)) {
            return $model;
        }

        throw new NotFoundMessageException();
    }

    public function getBannedMessages()
    {
        return $this->messageRepository->findBannedMessages();
    }
}