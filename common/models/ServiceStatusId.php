<?php

namespace common\models;

/**
 * This class is for service constants only
 */
class ServiceStatusId
{
    public const STATUS_NOT_ACTIVE = 1;
    public const STATUS_ACTIVE = 2;
    public const STATUS_BEFORE_ACTIVE = 3;

    public const STATUS_NOT_ACTIVE_LABEL = 'Not active';
    public const STATUS_ACTIVE_LABEL = 'Active';
    public const STATUS_BEFORE_ACTIVE_LABEL = 'Before active';

    public const STATUS_IDS_MAP = [
        self::STATUS_NOT_ACTIVE => self::STATUS_NOT_ACTIVE_LABEL,
        self::STATUS_ACTIVE => self::STATUS_ACTIVE_LABEL,
        self::STATUS_BEFORE_ACTIVE => self::STATUS_BEFORE_ACTIVE_LABEL,
    ];
}