<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chats".
 *
 * @property int $id Primary key
 * @property int|null $created_time Created time
 * @property int|null $updated_time Updated time
 * @property int|null $user_to
 * @property int|null $user_from
 *
 * @property Messages[] $messages
 */
class Chats extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'updated_time', 'user_to', 'user_from'], 'default', 'value' => null],
            [['created_time', 'updated_time', 'user_to', 'user_from'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Primary key'),
            'created_time' => Yii::t('app', 'Created time'),
            'updated_time' => Yii::t('app', 'Updated time'),
            'user_to' => Yii::t('app', 'User To'),
            'user_from' => Yii::t('app', 'User From'),
        ];
    }

    /**
     * Gets query for [[Messages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['chat_id' => 'id']);
    }
}
