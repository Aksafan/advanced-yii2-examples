<?php

namespace common\models\forms;

use yii\base\Model;

class CreateMessageForm extends Model
{
    public $message;
    public $date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'data'], 'required'],
            ['message', 'string'],
            ['date', 'int'],
        ];
    }
}