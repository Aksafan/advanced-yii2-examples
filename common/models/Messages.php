<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property int $id Primary key
 * @property string $message
 * @property int|null $created_time Created time
 * @property int|null $updated_time Updated time
 * @property int|null $user_to
 * @property int|null $user_from
 * @property int|null $chat_id
 *
 * @property Chats $chat
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['message'], 'string'],
            [['created_time', 'updated_time', 'user_to', 'user_from', 'chat_id'], 'default', 'value' => null],
            [['created_time', 'updated_time', 'user_to', 'user_from', 'chat_id'], 'integer'],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chats::className(), 'targetAttribute' => ['chat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Primary key'),
            'message' => Yii::t('app', 'Message'),
            'created_time' => Yii::t('app', 'Created time'),
            'updated_time' => Yii::t('app', 'Updated time'),
            'user_to' => Yii::t('app', 'User To'),
            'user_from' => Yii::t('app', 'User From'),
            'chat_id' => Yii::t('app', 'Chat ID'),
        ];
    }

    /**
     * Gets query for [[Chat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chats::className(), ['id' => 'chat_id']);
    }
}
