<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AdFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Ad';
}