<?php

use common\components\MessageService;
use common\components\TransactionComponent;
use common\repositories\MessageRepository;
use yii\di\Instance;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'transaction' => [
            'class' => TransactionComponent::class,
//            'url' => 'erherger',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=demo',
            'username' => 'root',
            'password' => '',
        ],
//        'userService' => [
//            'class' => \common\components\UserService::class,
//        ],
    ],
    'container' => [
        'definitions' => [
            'yii\widgets\LinkPager' => ['maxButtonCount' => 5]
        ],
        'singletons' => [
            MessageRepository::class,
            MessageService::class => [
                [],
                [
                    Instance::of(MessageRepository::class),
                ],
            ],
            \common\components\UserService::class => [
                [],
                [
                    Instance::of(\common\repositories\UserRepository::class),
                ],
            ],
        ],
    ],
];
