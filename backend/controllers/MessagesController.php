<?php

namespace backend\controllers;

use common\components\MessageService;
use common\exceptions\NotFoundMessageException;
use common\models\forms\CreateMessageForm;
use Yii;
use common\models\Messages;
use common\models\search\MessagesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MessagesController implements the CRUD actions for Messages model.
 */
class MessagesController extends Controller
{
    /**
     * @var $messageService MessageService
     */
    private $messageService;

    /**
     * MessagesController constructor.
     *
     * @param $id
     * @param $module
     * @param MessageService $messageService
     * @param array $config
     */
    public function __construct($id, $module, MessageService $messageService, $config = [])
    {
        $this->messageService = $messageService;

        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Messages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Messages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        try {
            $message = $this->messageService->getMessageById((int) $id);
        } catch (NotFoundMessageException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        return $this->render('view', [
            'model' => $message,
        ]);
    }

    /**
     * Creates a new Messages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreateMessageForm();

        if (
            $model->load(Yii::$app->request->post())
            && $model->validate()
            && $this->messageService->createMessage($model)
        ) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $languages = [];

        return $this->render('create', [
            'model' => $model,
            'languages' => $languages,
        ]);
    }

    /**
     * Updates an existing Messages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->messageService->getMessageById((int) $id);
        } catch (NotFoundMessageException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Messages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->messageService->getMessageById((int) $id);
        } catch (NotFoundMessageException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $model->delete();

        return $this->redirect(['index']);
    }
}
