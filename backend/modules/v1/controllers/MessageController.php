<?php

namespace backend\modules\v1\controllers;

use common\components\MessageService;
use common\models\Messages;
use common\models\search\MessagesSearch;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Message Controller for the `v1` module
 */
class MessageController extends Controller
{
    /**
     * @var $messageService MessageService
     */
    private $messageService;

    /**
     * MessagesController constructor.
     *
     * @param $id
     * @param $module
     * @param MessageService $messageService
     * @param array $config
     */
    public function __construct($id, $module, MessageService $messageService, $config = [])
    {
        $this->messageService = $messageService;

        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionIndex()
    {
        return ['id' => 1234];
    }

    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionView($id)
    {
        return ['id' => 1234];
    }
    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionCreate()
    {
        return ['id' => 1234];
    }
    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionUpdate($id)
    {
        return ['id' => 1234];
    }
    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionDelete($id)
    {
        return ['id' => 1234];
    }
    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionGetBannedMessages()
    {
        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search([]);


//        $messages = $this->messageService->getBannedMessages();
//
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $dataProvider;
    }
}
