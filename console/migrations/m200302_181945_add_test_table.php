<?php

use yii\db\Migration;

/**
 * Class m190723_180443_add_test_table
 */
class m200302_181945_add_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chats', [
            'id' => $this->primaryKey()->comment('Primary key'),
            'created_time' => $this->integer()->comment('Created time'),
            'updated_time' => $this->integer()->comment('Updated time'),
            'user_to' => $this->integer(),
            'user_from' => $this->integer(),
        ]);

        $this->createTable('messages', [
            'id' => $this->primaryKey()->comment('Primary key'),
            'message' => $this->text()->notNull(),
            'created_time' => $this->integer()->comment('Created time'),
            'updated_time' => $this->integer()->comment('Updated time'),
            'user_to' => $this->integer(),
            'user_from' => $this->integer(),
            'chat_id' => $this->integer(),
        ]);

        $this->addForeignKey('FK_chats_messages', 'messages', 'chat_id', 'chats', 'id', 'CASCADE');
        $this->createIndex('index_messages_chat_id', 'messages', 'chat_id');

        for ($i = 1; $i < 101; $i++) {
            $userTo = 10000 + $i;
            $userFrom = 10000 - $i;
            Yii::$app->db->createCommand(
                'INSERT INTO "chats" (created_time, updated_time, user_to, user_from) VALUES (:created_time, :updated_time, :user_to, :user_from)',
                [
                    ':created_time' => time(),
                    ':updated_time' => time(),
                    ':user_to' => $userTo,
                    ':user_from' => $userFrom,
                ]
            )
                ->execute();
            $k = 0;
            while ($k < 10) {
                Yii::$app->db->createCommand(
                    'INSERT INTO "messages" (message, created_time, updated_time, user_to, user_from, chat_id) VALUES (:message, :created_time, :updated_time, :user_to, :user_from, :chat_id)',
                    [
                        ':message' => 'Message # ' . $k . ' from user ' . $userFrom . ' to user ' . $userTo,
                        ':created_time' => time(),
                        ':updated_time' => time(),
                        ':user_to' => $userTo,
                        ':user_from' => $userFrom,
                        ':chat_id' => $i,
                    ]
                )
                    ->execute();

                $k++;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_chats_messages', 'messages');

        $this->dropTable('chats');
        $this->dropTable('messages');
    }
}
